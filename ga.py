import numpy as np
import random
import time
fitnessFunctionCalls = 0
tight = True
deceptive = False
ones = True
uniform = False
class Gene:
    length = 100
    bits = np.repeat(0, length)
    fitnessValue = 0
    global tight
    global deceptive
    global ones
    global uniform
    def __init__(self, random=False, ):
        if random:
            self.bits = np.random.randint(2, size=self.length)
            self.fitness()

    def fitness(self):
        global fitnessFunctionCalls
        fitnessFunctionCalls += 1
        if ones:
            self.fitnessValue = (self.bits == 1).sum()
        elif deceptive:
            self.fitnessValue = self.trapFunction(4, 1)
        else:
            self.fitnessValue = self.trapFunction(4, 2.5)

    def trapFunction(self, k, d):
        fitness = 0
        for i in range(int(self.length / k)):
            sectionCount = 0
            if tight:
                start = k*i
                end = k*(i+1)
                sectionCount = (self.bits[start:end] == 1).sum()
            else:
                indices = list(range(i, self.length, int(self.length / k)))
                sectionCount = (self.bits[indices] == 1).sum()
            if sectionCount == k:
                fitness += sectionCount
            else:
                fitness += k - d - ((k - d) / (k - 1)) * sectionCount
        return fitness
    
    def createOffspring(self, partner):
        offspring1 = Gene(random = False)
        offspring2 = Gene(random = False)
        offspring1.bits = np.copy(self.bits)
        offspring2.bits = np.copy(partner.bits)
        if uniform:
            crossoverProb = 0.5 #1 / self.length
            indices = np.array([x for x in list(range(self.length)) if (random.uniform(0, 1) <= crossoverProb)])
            if(len(indices) > 0):
                offspring1.bits[indices] = partner.bits[indices]
                offspring2.bits[indices] = self.bits[indices]
        else:
            points = [random.randint(0, 100), random.randint(0,100)]
            point1 = min(points)
            point2 = max(points)
            offspring1.bits[point1:point2] = partner.bits[point1:point2]
            offspring2.bits[point1:point2] = self.bits[point1:point2]
        offspring1.fitness()
        offspring2.fitness()
        return [offspring1, offspring2]


class Population:
    genes = []
    generation = 1
    populationSize = 250
    isConverged = False
    def __init__(self, populationSize):
        self.populationSize = populationSize
        self.genes = [Gene(random = True) for x in range(self.populationSize)]
    
    def shuffle(self):
        random.shuffle(self.genes)

    def generateOffspring(self):
        self.shuffle()
        newpopulation = []
        for i in range(0, self.populationSize, 2):
            offspring = []
            offspring = offspring + self.genes[i].createOffspring(self.genes[i + 1])
            offspring.append(self.genes[i])
            offspring.append(self.genes[i + 1])
            offspring = sorted(offspring, key=lambda x: x.fitnessValue, reverse=True)
            newpopulation += offspring[:2]
            if offspring[0].fitnessValue == 100:
                self.isConverged = True
        current = newpopulation[0].bits
        converged = True
        
        self.generation += 1
        self.genes = newpopulation
        for gene in newpopulation:
            if not np.all(gene.bits == current):
                converged = False
                return  converged
        return converged
        #self.bestFitness()
    
    def bestFitness(self):
        sortedList = sorted(self.genes, key=lambda x: x.fitnessValue, reverse=True)
        #print(self.generation)
        print("Generation: ", self.generation, sortedList[0].fitnessValue)


def run(number):
    amountConverged = 0
    generations = []
    initialPopulationSize = 80
    enoughConverged = False
    leftBound = 0
    rightBound = initialPopulationSize
    populationSize = initialPopulationSize
    reached24Convergence = False
    averagefunctionCalls = 0
    averageGenerations = 0
    averageRuntime = 0
    fail = False
    while not enoughConverged and not fail:
        if(populationSize > 2560):
            fail = True
            print("convergence failed")
            break
        functionCalls = 0
        totalGenerations = 0
        starttime = time.time()
        for run in range(25):
            print(f"Amount converged: {amountConverged}", f"Run: {run}")
            if(run - amountConverged >= 2):
                #Stop as soon as 2 have failed
                break
            population = Population(populationSize)
            functionCalls += populationSize

            for i in range(2500):
                done = population.generateOffspring()
                if(done):
                    print(f"Population converged at {population.generation} generations")
                    if(not population.isConverged):
                        break
                functionCalls += populationSize
                if population.isConverged:
                    amountConverged += 1
                    totalGenerations += i
                    break
        averageRuntime = (time.time() - starttime) / 25
        averageGenerations = totalGenerations / 25
        averagefunctionCalls = functionCalls / 25
        if(reached24Convergence):
            if(populationSize % 20 != 0):
                enoughConverged = True
                populationSize
            elif(amountConverged >= 24):
                rightBound = populationSize
                populationSize = int((leftBound + populationSize) / 2)
                print("Converge succesfull, adjust population size to:", populationSize)
            else:
                leftBound = populationSize
                populationSize = int((rightBound + populationSize) / 2)
                print("Converge failed, adjust population size to:", populationSize)
        else:
            if(amountConverged >= 24):
                reached24Convergence = True
                rightBound = populationSize
                populationSize = int((rightBound + leftBound) / 2)
                print("Converge for the first time decreased population size to:", populationSize)
            else:
                leftBound = rightBound
                rightBound = rightBound * 2
                populationSize = rightBound
                print("Converge failed, increased population size to:", populationSize)
        amountConverged = 0
    fileName = ""
    if(ones):
        fileName += "Ones"
    else:
        if deceptive:
            fileName += "Deceptive"
        else:
            fileName += "NonDeceptive"
        if(tight):
            fileName += "Tight"
        else:
            fileName += "NonTight"
    if(uniform):
        fileName+= "Uniform"
    else:
        fileName += "TwoPoint"
    f = open(fileName + ".txt", "w")
    if fail:
        f.write(f"Fail \n")
    else:
        f.write(f"Minimal population size: {populationSize} \n")
        f.write(f"Average function calls: {averagefunctionCalls} \n")
        f.write(f"Average generations: {averageGenerations} \n")
        f.write(f"Average run time: {averageRuntime} \n")
    f.close()
configs = [
    # {
    # "tight" : True,
    # "deceptive" : False,
    # "ones" : True,
    # },
    # {
    # "tight" : True,
    # "deceptive" : True,
    # "ones" : False,
    # },
    {
    "tight" : False,
    "deceptive" : True,
    "ones" : False,
    },
    # {
    # "tight" : True,
    # "deceptive": False,
    # "ones" : False,
    # },
    # {
    # "tight" : False,
    # "deceptive" : False,
    # "ones" : False,
    # },
]
number = 0
for config in configs:
    for crossoverType in [False]:
        number += 1
        tight = config["tight"]
        deceptive = config["deceptive"]
        ones = config["ones"]
        uniform = crossoverType
        run(number)
    print("completed run")